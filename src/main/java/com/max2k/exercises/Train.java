package com.max2k.exercises;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Train implements Serializable {

    List<RailVehicle> vagons=new ArrayList<>();
    String number;
    private String name;

    public Train() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Train(String number, String name) {
        this.number = number;
        this.name = name;
        //vagons = new ArrayList<>();
    }

    public void addVehicle(RailVehicle vehicle){
        vagons.add(vehicle);
    }


    @Override
    public String toString() {
        return "Train{" +
                "vagons=" + vagons +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public List<RailVehicle> getContent() {
        return new ArrayList<>(vagons);
    }
}
