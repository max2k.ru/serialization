package com.max2k.exercises;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class Serialisation {

    public static void main(String[] args) {
        Train t=new Train("T-900","Test train");
        t.addVehicle(new Vagon(4,60,Vagon.payloadType.CarCarrier));
        t.addVehicle(new Vagon(6,80,Vagon.payloadType.Container));
        t.addVehicle(new Vagon(4,30,Vagon.payloadType.Passenger));
        t.addVehicle(new Vagon(4,60,Vagon.payloadType.CarCarrier));

        List<RailVehicle> l=new ArrayList<>();
        l.add(new Vagon(4,60,Vagon.payloadType.CarCarrier));

        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("serdata.xml")));
            encoder.writeObject(t);
         //   encoder.writeObject(t.getContent());
         //   encoder.writeObject(l);
            encoder.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
