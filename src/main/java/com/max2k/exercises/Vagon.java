package com.max2k.exercises;

import java.io.Serializable;

public class Vagon implements RailVehicle, Serializable {
    protected int axilCount;
    protected int payLoadTonns;
    protected payloadType type;


    public enum payloadType {Container, Platform, CarCarrier,Passenger};

    public Vagon() {
    }

    public Vagon(int axilCount, int payLoadTonns, payloadType type) {
        this.axilCount = axilCount;
        this.payLoadTonns = payLoadTonns;
        this.type = type;
    }

    @Override
    public int getLength() {
        return 34;
    }

    public int getAxilCount() {
        return axilCount;
    }

    public void setAxilCount(int axilCount) {
        this.axilCount = axilCount;
    }

    public int getPayLoadTonns() {
        return payLoadTonns;
    }

    public void setPayLoadTonns(int payLoadTonns) {
        this.payLoadTonns = payLoadTonns;
    }

    public payloadType getType() {
        return type;
    }

    public void setType(payloadType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Vagon{" +
                "axilCount=" + axilCount +
                ", payLoadTonns=" + payLoadTonns +
                ", type=" + type +
                '}';
    }
}
