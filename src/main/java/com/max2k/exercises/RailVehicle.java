package com.max2k.exercises;

public interface RailVehicle {
     int getLength();
}
